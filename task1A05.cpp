#include <iostream>;

using namespace std;

int main() {
	double a1, b1, c1, a2, b2, c2, x, y;
	cin >> a1 >> b1 >> c1 >> a2 >> b2 >> c2;
	if ((a1 == 0 && b1 == 0) || (a2 == 0 && b2 == 0)) { cout << "no"; return 0; }
	else if (a1 == 0) { y = -c1 / b1; x = (-c2 - b2*y) / a2; }
	else if (b1 == 0) { x = -c1 / a1; y = (-c2 - a2*x) / b2; }
	else if (a2 == 0) { y = -c2 / b2; x = (-c1 - b1*y) / a1; }
	else if (b2 == 0) { x = -c2 / a2; y = (-c1 - a1*x) / b1; }
	else { y = (-c1*(-a2) - c2*a1) / (-a2*b1 + b2*a1); x = (-c1 - b1*y) / a1; }
	cout << x << " " << y << endl;
	system("PAUSE");

}