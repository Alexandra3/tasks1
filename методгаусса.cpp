#include <iostream>
using namespace std;
double g(double **a, int n) {             //M01
	double x = 0;
	for (int i = 0; i<n - 1; ++i) {
		for (int j = i + 1; j<n; j++) {
			x = a[j][i];
			for (int k = i; k<n; k++) {
				//x=a[j][k];

				a[j][k] = a[j][k] * a[0][i] / x - a[0][k];

			}

		}
	}
	for (int i = 0; i<n; ++i) {
		for (int j = 0; j<n; ++j) {
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	return 0;
}

double p(double **a, int n) {              //M02
	double t = 1;
	for (int i = 0; i < n; i++) {
		t = a[i][i] * t;
	}
	cout << t << endl;
	return 0;

}



int main() {
	int n;
	cin >> n;
	double **a = new double*[n];
	for (int i = 0; i<n; ++i) {
		a[i] = new double[n];
		for (int j = 0; j<n; ++j) {
			a[i][j] = rand() % 10 + 1;
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;

	g(a, n);

	system("pause");
	return 0;
}