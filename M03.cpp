#include <iostream>
#include <cmath>


using namespace std;

double det(double **a, int n) {
	double sum = 0;
	double **minor;
	int decr = 0;

	if (n == 1) return a[0][0];

	for (int i = 0; i < n; ++i) {
		minor = new double*[n - 1];
		for (int j = 0; j < n; ++j) {
			if (j == i) { decr = 1; continue; }
			minor[j - decr] = new double[n - 1];

			for (int k = 1; k < n; ++k) {
				minor[j - decr][k - 1] = a[j][k];

			}
		}
		decr = 0;
		sum += a[i][0] * pow(-1, i + 0)*det(minor, n - 1);
		for (int j = 0; j < n - 1; ++j) { delete minor[j]; }
		delete[] minor;
	}return sum;

}


int main() {
	int n;
	cin >> n;
	double **a = new double*[n];
	for (int i = 0; i<n; ++i) {
		a[i] = new double[n];
		for (int j = 0; j<n; ++j) {
			a[i][j] = rand() % 10 + 1;
			cout << a[i][j] << " ";
		}
		cout << endl;
	}
	cout << endl;

	cout << endl << det(a, n) << endl;


	system("pause");
	return 0;
}